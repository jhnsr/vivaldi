#!/bin/sh

if [ "$(id -un)" != 0 ]; then
	if [ -f /usr/bin/sudo ]; then
		echo "INFO: Using sudo for root operations."
		root="sudo"
	elif [ -f /usr/bin/doas ]; then
		echo "INFO: Using doas for root operations."
		root="doas"
	fi
fi

package=$(find $HOME/Downloads -type f -name "vivaldi*.deb" -exec basename {} \;)

# check if the Vivaldi package exist
if [ ! -f $HOME/Downloads/$package ]; then
	echo "There is no Vivaldi package :("
	exit 1
fi

# create build environment
echo; echo "Creating temporary working directory..."; echo

mkdir -pv $HOME/vivaldi-dev
cd $HOME/vivaldi-dev

# extract the deb package
echo; echo "Extracting archive..."

ar x $HOME/Downloads/$package

# create control and data folder
echo; echo "Creating folder to extract to..."; echo

mkdir -pv data

# extract control and data archives
echo; echo "Extracting files to data directory..."

tar xf data.tar.xz -C data

# move the data files and folders
echo; echo "Moving extracted directories to root directory..."

cd data
$root cp -r etc /
$root cp -r usr /
$root cp -r opt /

# clean up
echo; echo "Removing temporary working directory..."

$root rm -r $HOME/vivaldi-dev

echo; echo "Installation finished. Enjoy Vivaldi Browser."
